﻿namespace StudioKit.Repository.Interfaces
{
	public interface IVanityName
	{
		int Id { get; set; }
		string Name { get; set; }
		string VanityName { get; set; }
	}
}