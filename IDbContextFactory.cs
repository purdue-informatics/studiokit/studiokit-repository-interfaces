﻿using System;
using System.Data.Entity;

namespace StudioKit.Repository.Interfaces
{
	public interface IDbContextFactory : IDisposable
	{
		/// <summary>
		/// Get the factory
		/// </summary>
		/// <returns></returns>
		DbContext Context { get; }
	}
}