﻿namespace StudioKit.Repository.Interfaces
{
	public interface IModelState
	{
		void AddError(string key, string errorMessage);
	}
}